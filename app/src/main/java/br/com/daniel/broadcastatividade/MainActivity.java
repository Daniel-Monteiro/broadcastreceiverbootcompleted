package br.com.daniel.broadcastatividade;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocalBroadcastManager m = LocalBroadcastManager.getInstance(this);
        m.registerReceiver(new br.com.daniel.broadcastatividade.HelloReceiver(), new IntentFilter("BINGO"));
    }

    @Override
    public void onClick(View view) {
        sendBroadcast(new Intent("BINGO"));
        Toast.makeText(this,"Intentenviada!", Toast.LENGTH_SHORT).show();
    }

    public static class HelloReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("livroandroid", "br.com.daniel.broadcastatividade.MainActivity.HelloReceiver!!!");
        }
    }
}
